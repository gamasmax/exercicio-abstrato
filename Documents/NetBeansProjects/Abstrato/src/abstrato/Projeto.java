/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package abstrato;

import java.util.ArrayList;

/**
 *
 * @author Raynan
 */
public class Projeto {
    private Data dataInicio = new Data();
    private Data dataFim = new Data();
    private ArrayList<Funcionario> List = new ArrayList<Funcionario>();
    private Funcionario lider;
    private boolean concluido;
    public void addFuncionario(Funcionario a){
        List.add(a);
    }
    public void finalizarProjeto(){
        System.out.println("Projeto pronto :)");
    }
    public void trocarLider(Funcionario I){
        this.lider=I;
    }
    public void dataInicio(int d, int m, int a){
        dataInicio.setAno(a);
        dataInicio.setMes(m);
        dataInicio.setDia(d);
    }
    public void dataFim(int d, int m, int a){
        dataFim.setAno(a);
        dataFim.setMes(m);
        dataFim.setDia(d);
    }
}
